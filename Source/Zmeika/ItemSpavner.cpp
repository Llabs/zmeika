// Fill out your copyright notice in the Description page of Project Settings.


#include "ItemSpavner.h"
#include "Engine/Classes/Components/BoxComponent.h"
#include "Food.h"
#include "SnakeSpeedBonus.h"
#include "Blokc.h"
#include "Kismet/KismetMathLibrary.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AItemSpavner::AItemSpavner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpawnBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnBox"));
	SpawnBoxComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	MaxItem = 5;
	ItemOnMap = 0;

}

// Called when the game starts or when spawned
void AItemSpavner::BeginPlay()
{
	Super::BeginPlay();
	ItemSpawn(5);
	GetWorld()->GetTimerManager().SetTimer(SpavnTimer, this, &AItemSpavner::SpavnOnTimer, 4.0f, true);
}

// Called every frame
void AItemSpavner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AItemSpavner::ItemSpawn(int32 ItemCount)
{
	FVector ZoneOrigin, ZoneExtenBox, SpawnPoint;
	int Cheker;

	ZoneOrigin = AItemSpavner::GetActorLocation();
	ZoneExtenBox = SpawnBoxComponent->GetScaledBoxExtent();

	for (int i = 0; i < ItemCount; i++)
	{
		if (ItemOnMap < MaxItem)
		{
			Cheker = FMath::RandRange(0, 20);
			SpawnPoint = UKismetMathLibrary::RandomPointInBoundingBox(ZoneOrigin, ZoneExtenBox);
			SpawnPoint.Z = 0;
			FTransform SpawnTransform(SpawnPoint);
			if (Cheker>14)
			{
				ASnakeSpeedBonus* NewSpeedBonus = GetWorld()->SpawnActor<ASnakeSpeedBonus>(SpeedBonusClass, SpawnTransform);
				NewSpeedBonus->SetItemSpavnerRef(this);
			}
			else
			{
				AFood* NewFood = GetWorld()->SpawnActor<AFood>(FoodClass, SpawnTransform);
				NewFood->SetItemSpavnerRef(this);
			}
			ItemOnMap++;
	    }
	}
}

void AItemSpavner::BlokcSpavn()
{
	FVector ZoneOrigin, ZoneExtenBox, SpawnPoint;
	int Cheker;

	Cheker = FMath::RandRange(0, 2);
	ZoneOrigin = AItemSpavner::GetActorLocation();
	ZoneExtenBox = SpawnBoxComponent->GetScaledBoxExtent();

	SpawnPoint = UKismetMathLibrary::RandomPointInBoundingBox(ZoneOrigin, ZoneExtenBox);
	SpawnPoint.Z = 0;
	FTransform SpawnTransform(SpawnPoint);

	

	switch (Cheker)
	{
	case 0:
		GetWorld()->SpawnActor<ABlokc>(ABlokcClass1, SpawnTransform);
		break;
	case 1:
		GetWorld()->SpawnActor<ABlokc>(ABlokcClass2, SpawnTransform);
		break;
	case 2:
		GetWorld()->SpawnActor<ABlokc>(ABlokcClass2, SpawnTransform);
		break;
	}

}

void AItemSpavner::SpavnOnTimer()
{
	ItemSpawn();
	BlokcSpavn();
}

void AItemSpavner::RemoweSpawnItem()
{
	ItemOnMap--;
}


