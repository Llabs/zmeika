// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeSpeedBonus.h"
#include "SnakeBase.h"
#include "ItemSpavner.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeSpeedBonus::ASnakeSpeedBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnDestroy = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void ASnakeSpeedBonus::BeginPlay()
{
	Super::BeginPlay();
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &ASnakeSpeedBonus::HandleBeginOwerlap);
	GetWorld()->GetTimerManager().SetTimer(LockTimer, this, &ASnakeSpeedBonus::CloseSpawnDestrou, 0.1f, true);
}

// Called every frame
void ASnakeSpeedBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeSpeedBonus::interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->ActivateSpeedBonus();
			if (IsValid(ItemSpavnerRef))
			{
				ItemSpavnerRef->RemoweSpawnItem();
			}

			MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			MeshComponent->SetVisibility(false, false);
			GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &ASnakeSpeedBonus::DestroyFunc, 1.0f, true);
		}
	}
}

void ASnakeSpeedBonus::SetItemSpavnerRef(AItemSpavner* Reference)
{
	ItemSpavnerRef = Reference;
}

void ASnakeSpeedBonus::CloseSpawnDestrou()
{
	SpawnDestroy = false;
}

void ASnakeSpeedBonus::DestroyFunc()
{
	this->Destroy();
}

void ASnakeSpeedBonus::HandleBeginOwerlap(UPrimitiveComponent* OverlapedComponent,
	                                      AActor* OtherActor, 
	                                      UPrimitiveComponent* OtherComponent, 
	                                      int32 OtherBodyIndex, 
	                                      bool bFromSweep, 
	                                      const FHitResult& SweepResult)
{
	if (SpawnDestroy)
	{
		if (IsValid(ItemSpavnerRef))
		{
			ItemSpavnerRef->RemoweSpawnItem();
		}

		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		MeshComponent->SetVisibility(false, false);
		GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &ASnakeSpeedBonus::DestroyFunc, 1.0f, true);

	}		
}
