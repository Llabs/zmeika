// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Interactable.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	bSpeedBonusActive = false;

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnekeElement(5);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
    Move();

}

void ASnakeBase::AddSnekeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		int LastItem = SnakeElements.Num() - 1;
		FVector NevLocation;
		if (LastItem > 0)
		{
			FVector LastElementLocation = SnakeElements[LastItem]->GetActorLocation();

			switch (LastMoveDirection)
			{
			case EMovementDirection::UP:
				LastElementLocation.X -= ElementSize;
				break;
			case EMovementDirection::DOWN:
				LastElementLocation.X += ElementSize;
				break;
			case EMovementDirection::LEFT:
				LastElementLocation.Y -= ElementSize;
				break;
			case EMovementDirection::RIGHT:
				LastElementLocation.Y += ElementSize;
				break;
			}
			NevLocation = LastElementLocation;
		}
		else
		{
			FVector LastElementLocation(SnakeElements.Num() * ElementSize, 0, 0);
			NevLocation = LastElementLocation;
		}

		FTransform NevTransform(NevLocation);
		ASnakeElementBase* NewSnekeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NevTransform);
		NewSnekeElem->SnakeOwener = this;
		int32 IndexElement = SnakeElements.Add(NewSnekeElem);
		
		if (IndexElement == 0)
		{
			NewSnekeElem->SetFirstElementType();
			
		}
		
	}

}

void ASnakeBase::ActivateSpeedBonus()
{
	if (bSpeedBonusActive == false)
	{
		bSpeedBonusActive = true;
		MovementSpeed = MovementSpeed / 5;
		SetActorTickInterval(MovementSpeed);
		GetWorld()->GetTimerManager().SetTimer(SpeedBonusHandle, this, &ASnakeBase::DisActivateSpeedBonus, 10.0f, false);
	}
}

void ASnakeBase::DisActivateSpeedBonus()
{
	bSpeedBonusActive = false;
	MovementSpeed = MovementSpeed * 5;
	SetActorTickInterval(MovementSpeed);
	GetWorldTimerManager().ClearTimer(SpeedBonusHandle);
}

void ASnakeBase::Move()
{

	FVector MovementVector(ForceInitToZero);
	float Movement = ElementSize;

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += Movement;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= Movement;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += Movement;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y -= Movement;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];

		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlapedElement, AActor* Other)
{
	if (IsValid(OverlapedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlapedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->interact(this, bIsFirst);
		}
	}
}
