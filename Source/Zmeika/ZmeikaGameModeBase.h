// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ZmeikaGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ZMEIKA_API AZmeikaGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
