// Fill out your copyright notice in the Description page of Project Settings.


#include "BorderActor.h"
#include "SnakeBase.h"

// Sets default values
ABorderActor::ABorderActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABorderActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABorderActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABorderActor::interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
		}
	}
}

