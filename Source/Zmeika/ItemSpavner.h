// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ItemSpavner.generated.h"


class UBoxComponent;
class AFood;
class ASnakeSpeedBonus;
class ABlokc;

UCLASS()
class ZMEIKA_API AItemSpavner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItemSpavner();

	UPROPERTY(EditDefaultsOnly)
		int32 MaxItem;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UBoxComponent* SpawnBoxComponent;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeSpeedBonus> SpeedBonusClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlokc> ABlokcClass1;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlokc> ABlokcClass2;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ABlokc> ABlokcClass3;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	int32 ItemOnMap;
	FTimerHandle SpavnTimer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ItemSpawn(int32 ItemCount = 1);
	void BlokcSpavn();
	void SpavnOnTimer();
	void RemoweSpawnItem();

};