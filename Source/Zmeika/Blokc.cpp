// Fill out your copyright notice in the Description page of Project Settings.


#include "Blokc.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABlokc::ABlokc()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnDestroy = true;

	MeshComponent1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("FirstMeshComponent"));
	MeshComponent1->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent1->SetCollisionResponseToAllChannels(ECR_Overlap);

	MeshComponent2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SecondMeshComponent"));
	MeshComponent2->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent2->SetCollisionResponseToAllChannels(ECR_Overlap);
}


// Called when the game starts or when spawned
void ABlokc::BeginPlay()
{
	Super::BeginPlay();

	float timelife;
	timelife = FMath::RandRange(10.0f, 25.0f);
	GetWorld()->GetTimerManager().SetTimer(TimeLifeHandle, this, &ABlokc::LifeEnd, timelife, false);
	GetWorld()->GetTimerManager().SetTimer(LockTimer, this, &ABlokc::CloseSpawnDestrou, 0.1f, true);


	MeshComponent1->OnComponentBeginOverlap.AddDynamic(this, &ABlokc::HandleBeginOwerlap);
	MeshComponent2->OnComponentBeginOverlap.AddDynamic(this, &ABlokc::HandleBeginOwerlap);
	
}

// Called every frame
void ABlokc::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABlokc::interact(AActor* interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(interactor);
	if (IsValid(Snake))
	{
		Snake->Destroy();
	}
}

void ABlokc::LifeEnd()
{
	Destroy();
}

void ABlokc::CloseSpawnDestrou()
{
	SpawnDestroy = false;
}

void ABlokc::HandleBeginOwerlap(UPrimitiveComponent* OverlapedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (SpawnDestroy)
	{
		this->Destroy();
	}
}
