// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Blokc.generated.h"


class UStaticMeshComponent;

UCLASS()
class ZMEIKA_API ABlokc : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABlokc();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent1;

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent2;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	bool SpawnDestroy;
	FTimerHandle LockTimer;
	FTimerHandle TimeLifeHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void interact(AActor* interactor, bool bIsHead) override;
	void LifeEnd();
	void CloseSpawnDestrou();

	UFUNCTION()
		void HandleBeginOwerlap(UPrimitiveComponent* OverlapedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

};
