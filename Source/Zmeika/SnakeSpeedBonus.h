// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeSpeedBonus.generated.h"

class AItemSpavner;
class UStaticMeshComponent;

UCLASS()
class ZMEIKA_API ASnakeSpeedBonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeSpeedBonus();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;
	


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY()
		AItemSpavner* ItemSpavnerRef;

	bool SpawnDestroy;
	FTimerHandle LockTimer;
	FTimerHandle DestroyTimer;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void interact(AActor* interactor, bool bIsHead) override;
	void SetItemSpavnerRef(AItemSpavner* Reference);
	void CloseSpawnDestrou();
	void DestroyFunc();

	UFUNCTION()
		void HandleBeginOwerlap(UPrimitiveComponent* OverlapedComponent,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult);

};
