// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "ItemSpavner.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"


// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SpawnDestroy = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();

	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AFood::HandleBeginOwerlap);
	GetWorld()->GetTimerManager().SetTimer(LockTimer, this, &AFood::CloseSpawnDestrou, 0.1f, true);
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AFood::interact(AActor* interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnekeElement();
			if (IsValid(ItemSpavnerRef))
			{
				ItemSpavnerRef->RemoweSpawnItem();
			}
			
			MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
			MeshComponent->SetVisibility(false, false);
			GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &AFood::DestroyFunc, 1.0f, true);

		}
	}
}

void AFood::SetItemSpavnerRef(AItemSpavner* Reference)
{
	ItemSpavnerRef = Reference;
}

void AFood::CloseSpawnDestrou()
{
	SpawnDestroy = false;
}

void AFood::DestroyFunc()
{
	this->Destroy();
}

void AFood::HandleBeginOwerlap(UPrimitiveComponent* OverlapedComponent,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComponent,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult)
{
	if (SpawnDestroy)
	{
		if (IsValid(ItemSpavnerRef))
		{
			ItemSpavnerRef->RemoweSpawnItem();
		}
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		MeshComponent->SetVisibility(false, false);
		GetWorld()->GetTimerManager().SetTimer(DestroyTimer, this, &AFood::DestroyFunc, 1.0f, true);
	}
}

